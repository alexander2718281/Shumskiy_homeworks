package ru.mail.alexander_kurlovich3;

public class Main {

    public static void main(String[] args) {
        printDaysInYearPeriod(2000, 2010);
        printSuareOfConsecutiveNumbers(10, 20);
        int[] array = new int[31];
        for (int i = 0; i < array.length; i++){
            array[i] = 20+i;
        }
        printNumsAliquot3Aliquant5(array);
    }

    public static void printDaysInYearPeriod(int firstYear, int lastYear){
        int start;
        int end;
        if(firstYear >= 0 && lastYear >= 0){
            if(firstYear <= lastYear){
                start = firstYear;
                end = lastYear;
            }
            else {
                start = lastYear;
                end = firstYear;
            }
        }
        else {
            System.out.println("This method don't process years BC");
            return;
        }
        while (start <= end){
            System.out.println("In " + start + " " + daysInTheYear(start)  + " days");
            start++;
        }
    }

    public static int daysInTheYear(int year){
        if (year % 4 == 0){
            if (year % 100 == 0){
                if (year % 400 == 0){
                    return 366;
                }
                return 365;
            }
            return 366;
        }
        return 365;
    }

    public static void printSuareOfConsecutiveNumbers(int firstNum, int lastNum){
        int start;
        int end;
        if(firstNum <= lastNum){
            start = firstNum;
            end = lastNum;
        }
        else {
            start = lastNum;
            end = firstNum;
        }
        while (start <= end){
            System.out.println(start*start);
            start++;
        }
    }

    public static void printNumsAliquot3Aliquant5(int[] array){
        System.out.println("Task:3");
        for (int i : array) {
            if (i % 3 == 0 && i % 5 !=0 )
                System.out.println(i);
        }
    }
}


