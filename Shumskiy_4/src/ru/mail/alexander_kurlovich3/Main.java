package ru.mail.alexander_kurlovich3;

public class Main {

    public static void main(String[] args) {
	    int[] firstArray = {34, 65, 13, 7};
	    int[] secondArray = {54, 12, 7, 94};
	    int[] thirdArray = new int[8];
	    thirdArray[0] = firstArray[0];
	    thirdArray[1] = firstArray[1];
	    thirdArray[2] = firstArray[2];
	    thirdArray[3] = firstArray[3];
	    thirdArray[4] = secondArray[0];
	    thirdArray[5] = secondArray[1];
	    thirdArray[6] = secondArray[2];
	    thirdArray[7] = secondArray[3];
		System.out.println("In the year " + daysInTheYear(2018) + " days");
		timeOfTheYear(3);
    }

    public static int daysInTheYear(int year){
		if (year % 4 == 0){
			if (year % 100 == 0){
				if (year % 400 == 0){
					return 366;
				}
				return 365;
			}
			return 366;
		}
		return 365;
	}

	public static void timeOfTheYear(int sesson){
    	switch (sesson){
			case 1:
				System.out.println("Winter");
				break;
			case 2:
				System.out.println("Spring");
				break;
			case 3:
				System.out.println("Summer");
				break;
			case 4:
				System.out.println("Autumn");
				break;
			default:
				System.out.println("Unknown time of the year");
		}
	}
}
