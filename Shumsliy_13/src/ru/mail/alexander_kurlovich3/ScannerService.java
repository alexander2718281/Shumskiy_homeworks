package ru.mail.alexander_kurlovich3;

import java.util.Scanner;

public class ScannerService {
    private Scanner scanner = new Scanner(System.in);

    public int getIntFromScanner(){
        return scanner.nextInt();
    }

    public int[] getIntArrayFromScanner(int numOfElements){
        int[] array = new int[numOfElements];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Enter element of array:");
            array[i] = getIntFromScanner();
        }
        return array;
    }
}
