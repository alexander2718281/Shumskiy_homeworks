package ru.mail.alexander_kurlovich3.task3;

public class StringSortService {

    public String deleteSomeWords(String text, int minSimvols, int maxSimvols) {
        String[] strings = text.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String string : strings) {
            if (string.length() < 3 || string.length() > 5) {
                sb.append(string).append(' ');
            }
        }
        return sb.toString();
    }
}
