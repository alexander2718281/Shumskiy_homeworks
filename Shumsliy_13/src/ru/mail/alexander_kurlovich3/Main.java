package ru.mail.alexander_kurlovich3;


import ru.mail.alexander_kurlovich3.task1.InputService;
import ru.mail.alexander_kurlovich3.task1.SortService;
import ru.mail.alexander_kurlovich3.task2.OutputService;
import ru.mail.alexander_kurlovich3.task3.InputReaderService;
import ru.mail.alexander_kurlovich3.task3.StringSortService;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        //task1
        InputService inputService = new InputService();
        SortService sortService = new SortService();
        System.out.println(sortService.findMinimalElementInArray(inputService.getIntArrayFromFile("task1file.txt")));


        //task2
        System.out.println("Enter the size of array:");
        ScannerService scanner = new ScannerService();
        int arraylenght = scanner.getIntFromScanner();
        OutputService outputService = new OutputService();
        outputService.writeIntArrayOnFile(scanner.getIntArrayFromScanner(arraylenght), "task2.txt");


        //task3
        File folder = new File("newFolder");
        folder.mkdir();
        File file = new File(folder.getAbsolutePath(), "newFile.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputReaderService inputReaderService = new InputReaderService();
        StringSortService stringSortService = new StringSortService();
        String redactedText = stringSortService.deleteSomeWords(inputReaderService.readText("task3.txt"), 3, 5);
        try (FileWriter fr = new FileWriter(file);
             BufferedWriter br = new BufferedWriter(fr);) {
            br.write(redactedText, 0, redactedText.length());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
