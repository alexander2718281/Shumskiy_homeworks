package ru.mail.alexander_kurlovich3.task1;

public class SortService {

    public int findMinimalElementInArray(int[] ints){
        int min = ints[0];
        for (int anInt : ints) {
            if (anInt < min){
                min = anInt;
            }
        }
        return min;
    }
}
