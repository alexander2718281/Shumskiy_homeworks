package ru.mail.alexander_kurlovich3.task1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InputService {

    public int[] getIntArrayFromFile(String str){
        List<String> listOfStrings = new ArrayList<>();
        String string;
        try (FileReader fr = new FileReader(str);
             BufferedReader br = new BufferedReader(fr)) {
            string = br.readLine();
            while (string != null){
                listOfStrings.add(string);
                string = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int[] ints = new int[listOfStrings.size()];
        String st;
        for (int i = 0; i < ints.length; i++) {
            st = listOfStrings.get(i).trim();
            ints[i] = Integer.parseInt(st);
        }
        return ints;
    }
}
