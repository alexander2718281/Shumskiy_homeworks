package ru.mail.alexander_kurlovich3.task2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class OutputService {

    public void writeIntArrayOnFile(int[] array, String fileAddress) {
        String[] strings = new String[array.length];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = String.valueOf(array[i]);
        }
        try (FileWriter fw = new FileWriter(fileAddress);
             BufferedWriter bw = new BufferedWriter(fw)) {
            for (int i = 0; i < strings.length; i++) {
                bw.write(String.format("%s \n", strings[i]), 0, strings[i].length() + 2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
