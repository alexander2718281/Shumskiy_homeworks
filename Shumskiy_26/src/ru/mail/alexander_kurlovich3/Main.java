package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.dao.BookDao;
import ru.mail.alexander_kurlovich3.dao.ClientDao;
import ru.mail.alexander_kurlovich3.dao.impl.BookDaoImpl;
import ru.mail.alexander_kurlovich3.dao.impl.ClientDaoImpl;
import ru.mail.alexander_kurlovich3.dao.model.Book;
import ru.mail.alexander_kurlovich3.dao.model.Client;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        BookDao bookDao = new BookDaoImpl();
        ClientDao clientDao = new ClientDaoImpl();
        clientDao.dropClientTable();
        bookDao.dropBookTable();
        clientDao.cteateClientTable();
        bookDao.createBookTable();
        List[] lists = GeneratorService.getInstance().generateBooksAndClients(3, 15);
        List<Client> clients = lists[0];
        List<Book> books = lists[1];
        for (Client client : clients) {
            clientDao.save(client);
        }
        for (Book book : books) {
            bookDao.save(book);
        }
        List<Book> endBooks = bookDao.getBooks();
        for (Book endBook : endBooks) {
            System.out.println(endBook);
        }
    }
}
