package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.dao.model.Book;
import ru.mail.alexander_kurlovich3.dao.model.Client;

import java.util.ArrayList;
import java.util.List;

public class GeneratorService {
    private static GeneratorService service;

    private GeneratorService(){}

    public static GeneratorService getInstance(){
        if (service == null){
            service = new GeneratorService();
        }
        return service;
    }

    public List[] generateBooksAndClients(int numOfClients, int numOfBooks){
        List[] lists = new List[2];
        lists[0] = new ArrayList();
        lists[1] = new ArrayList();
        for (int i = 0; i < numOfClients; i++) {
            Client client = new Client("name" +i);
            lists[0].add(client);
            for (int j = 0; j < numOfBooks/numOfClients; j++) {
                int number = j + i*(numOfBooks/numOfClients);
                lists[1].add(new Book("title" + number, client));
            }
        }
        return lists;
    }
}
