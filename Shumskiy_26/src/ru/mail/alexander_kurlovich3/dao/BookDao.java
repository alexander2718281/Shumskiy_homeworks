package ru.mail.alexander_kurlovich3.dao;

import ru.mail.alexander_kurlovich3.dao.model.Book;
import ru.mail.alexander_kurlovich3.dao.model.Client;

import java.util.List;
import java.util.Map;

public interface BookDao {
    void createBookTable();
    void dropBookTable();
    int save(Book book);
    List<Book> getBooks();
}
