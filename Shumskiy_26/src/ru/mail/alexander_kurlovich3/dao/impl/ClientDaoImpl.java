package ru.mail.alexander_kurlovich3.dao.impl;

import ru.mail.alexander_kurlovich3.dao.ClientDao;
import ru.mail.alexander_kurlovich3.dao.connection.ConnectorService;
import ru.mail.alexander_kurlovich3.dao.model.Client;

import java.sql.*;

public class ClientDaoImpl implements ClientDao {
    @Override
    public void cteateClientTable() {
        String createTableSQL = "CREATE TABLE IF NOT EXISTS db_client("
                + "id INTEGER (11) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                + "name VARCHAR(30) NOT NULL"
                + ")";
        execute(createTableSQL);
    }

    @Override
    public void dropClientTable() {
        String dropTableSQL = "DROP TABLE IF EXISTS db_client";
        execute(dropTableSQL);
    }

    @Override
    public int save(Client client) {
        String sql = "INSERT INTO db_client (name) VALUES(?);";
        Connection connection = ConnectorService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, client.getName());
                int i = preparedStatement.executeUpdate();
                ResultSet rs = preparedStatement.getGeneratedKeys();
                if (rs.next()){
                    client.setId(rs.getLong(1));
                }
                return i;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }

        return 0;
    }

    @Override
    public Client getById(long id) {
        String selectSQL = "SELECT * FROM db_client WHERE id = ?;";
        Connection connection = ConnectorService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSQL)) {
                preparedStatement.setLong(1, id);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    rs.next();
                    Client client = new Client(rs.getLong("id"), rs.getString("name"));
                    return client;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    private void execute(String sql) {
        Connection connection = ConnectorService.getConnection();
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(sql);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }
}
