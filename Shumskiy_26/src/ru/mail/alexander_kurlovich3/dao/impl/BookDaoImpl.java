package ru.mail.alexander_kurlovich3.dao.impl;

import ru.mail.alexander_kurlovich3.dao.BookDao;
import ru.mail.alexander_kurlovich3.dao.connection.ConnectorService;
import ru.mail.alexander_kurlovich3.dao.model.Book;
import ru.mail.alexander_kurlovich3.dao.model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookDaoImpl implements BookDao {


    @Override
    public void createBookTable() {
        String createTableSQL = "CREATE TABLE IF NOT EXISTS db_book("
                + "id INTEGER (11) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                + "title VARCHAR(100) NOT NULL, "
                + "client_id INTEGER (11) NOT NULL"
                + ")";
        execute(createTableSQL);
    }

    @Override
    public void dropBookTable() {
        String dropTableSQL = "DROP TABLE IF EXISTS db_book";
        execute(dropTableSQL);
    }

    @Override
    public int save(Book book) {
        String sql = "INSERT INTO db_book(title, client_id) VALUES(?, ?);";
        Connection connection = ConnectorService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, book.getTitle());
                preparedStatement.setLong(2, book.getClient().getId());
                return preparedStatement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
        return 0;
    }

    @Override
    public List<Book> getBooks() {
        String selectAllBooksSQL = "SELECT bk.id, title, cl.id, name FROM db_book as bk JOIN db_client AS cl ON bk.client_id = cl.id;";
        Connection connection = ConnectorService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectAllBooksSQL)) {
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    List<Book> books = new ArrayList<>();
                    Map<Long, Client> clients = new HashMap<>();
                    while (rs.next()) {
                        long client_id = rs.getLong("cl.id");
                        Client client;
                        if (clients.containsKey(client_id)) {
                            client = clients.get(client_id);

                        } else {
                            client = new Client(client_id, rs.getString("name"));
                            clients.put(client_id, client);
                        }
                        books.add(new Book(rs.getLong("bk.id"), rs.getString("title"), client));
                    }
                    return books;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    private void execute(String sql) {
        Connection connection = ConnectorService.getConnection();
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(sql);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }
}
