package ru.mail.alexander_kurlovich3.dao.model;

import ru.mail.alexander_kurlovich3.dao.model.Client;

public class Book {

    private long id;
    private String title;
    private Client client;

    public Book(long id, String title, Client client) {
        this.id = id;
        this.title = title;
        this.client = client;
    }

    public Book(String title, Client client) {
        this.title = title;
        this.client = client;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Book{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", client=").append(client);
        sb.append('}');
        return sb.toString();
    }
}
