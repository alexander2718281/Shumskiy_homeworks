package ru.mail.alexander_kurlovich3.dao;

import ru.mail.alexander_kurlovich3.dao.model.Client;

public interface ClientDao {

    void cteateClientTable();
    void dropClientTable();
    int save (Client client);
    Client getById(long id);
}
