package ru.mail.alexander_kurlovich3.task3;

public class PrintService extends Thread {
    private PrintNameAndTimeService printNameAndTimeService;

    public PrintService(PrintNameAndTimeService printNameAndTimeService) {
        this.printNameAndTimeService = printNameAndTimeService;
    }

    @Override
    public void run() {
        printNameAndTimeService.printThreadNameAndTime();
    }
}
