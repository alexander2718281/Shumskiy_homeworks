package ru.mail.alexander_kurlovich3.task3;

import java.util.Date;

public class PrintNameAndTimeService {

    public synchronized void printThreadNameAndTime(){
        System.out.println("Thread name - " + Thread.currentThread().getName());
        Date date = new Date();
        System.out.println("Current time - " + date.toString());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
