package ru.mail.alexander_kurlovich3.task2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class ArrayWriteInFileThread extends Thread {
    @Override
    public void run() {
        int[] array = getRandomArray(10);
        try (FileWriter fw = new FileWriter("File of " + getName() + ".txt");
             BufferedWriter bw = new BufferedWriter(fw)) {
            for (int i : array) {
                bw.write(i + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int[] getRandomArray(int arrayLenght) {
        int[] randomArray = new int[arrayLenght];
        Random rnd = new Random();
        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = rnd.nextInt();
        }
        return randomArray;
    }
}
