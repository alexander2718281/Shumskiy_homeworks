package ru.mail.alexander_kurlovich3.task1;

import java.util.Random;

public class ArrayGenereteThread extends Thread {
    @Override
    public void run() {
        int numOfElements = 10;
        int[] randomArray = new int[numOfElements];
        Random rnd = new Random();
        randomArray[0] = rnd.nextInt();
        int maxValue = randomArray[0];
        for (int i = 1; i < randomArray.length; i++) {
            randomArray[i] = rnd.nextInt(1000);
            if (randomArray[i] > maxValue){
                maxValue = randomArray[i];
            }
        }
        System.out.println(Thread.currentThread().getName() + ": " + maxValue);
    }
}
