package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.ArrayGenereteThread;
import ru.mail.alexander_kurlovich3.task2.ArrayWriteInFileThread;
import ru.mail.alexander_kurlovich3.task3.PrintNameAndTimeService;
import ru.mail.alexander_kurlovich3.task3.PrintService;

public class Main {

    public static void main(String[] args) {
        //task1
        int numOfThreads = 10;
        for (int i = 0; i < numOfThreads; i++) {
            Thread arrayThread = new ArrayGenereteThread();
            arrayThread.setName("Thread" + i);
            arrayThread.start();
        }

        //task2
        int numOfFileThreads = 5;
        for (int i = 0; i < numOfFileThreads; i++) {
            Thread fileThread = new ArrayWriteInFileThread();
            fileThread.setName("Thread" + i);
            fileThread.start();
        }

        //task3
        int numOfPrintThreads = 10;
        PrintNameAndTimeService printNameAndTimeService = new PrintNameAndTimeService();
        for (int i = 0; i < numOfPrintThreads; i++) {
            Thread printServiceThread = new PrintService(printNameAndTimeService);
            printServiceThread.setName("Thread " + i);
            printServiceThread.start();
        }
    }
}
