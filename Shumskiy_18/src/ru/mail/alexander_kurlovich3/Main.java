package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.PracticeOnThreadsTask2.ThreadForStartOtherThread;
import ru.mail.alexander_kurlovich3.task1.CallableStringGenerateService;
import ru.mail.alexander_kurlovich3.task2.Consumer;
import ru.mail.alexander_kurlovich3.task2.Producer;
import ru.mail.alexander_kurlovich3.task2.TaskData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {
	//task1
        int numOfThreads = 5;
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        Callable<String> callableGenerateService = new CallableStringGenerateService(5,10, 'b', 'v', 'x');
        List<Future<String>> listOfFutureStrings = new ArrayList<>();
        for (int i = 0; i < numOfThreads; i++) {
            listOfFutureStrings.add(executorService.submit(callableGenerateService));
        }
        executorService.shutdown();
        List<String> listOfMaxStrings = sortFututeListWithLongestStrings(listOfFutureStrings);
        System.out.println("Max length = " + listOfMaxStrings.get(0).length());
        System.out.println("Max lines:");
        for (String line : listOfMaxStrings) {
            System.out.println(line);
        }
     //task2

        ExecutorService executorService1 = Executors.newFixedThreadPool(4);
        TaskData data = new TaskData();
        executorService1.submit(new Producer(data, 10));
        executorService1.submit(new Producer(data, 10));
        executorService1.submit(new Consumer(data));
        executorService1.submit(new Consumer(data));
        executorService1.shutdown();

        //Practice on threads task2
        // Напишите программу, которая создает поток (назовем его Thread 1). Thread 1 создает другой поток (Thread 2); Thread 2 создает Thread 3; и т. д., вплоть до Thread 50.
        // Каждый поток должен печатать «Hello from Thread <num>!», но вы должны структурировать свою программу таким образом, чтобы потоки печатали свои приветствия в обратном порядке.
        Object o = new Object();
        Thread thread = new ThreadForStartOtherThread(1, o);
        thread.start();
    }


    private static List<String> sortFututeListWithLongestStrings(List<Future<String>> listOfFutures){
        String line = null;
        List<String> listOfStrings = new ArrayList<>();
        int maxLenght = 0;
        for (Future<String> future : listOfFutures) {
            try {
                line = future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if (line.length() > maxLenght){
                maxLenght = line.length();
                listOfStrings.clear();
                listOfStrings.add(line);
            }
            else if (line.length() == maxLenght){
                listOfStrings.add(line);
            }
        }
        return listOfStrings;
    }
}
