package ru.mail.alexander_kurlovich3.task2;

import java.util.Random;

public class Producer implements Runnable{
    private TaskData data;
    private int count;
    private Random random = new Random();

    public Producer(TaskData data, int count) {
        this.data = data;
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("Producer thread started");
        for (int i = 0; i < count; i++) {
            data.putInt(random.nextInt());
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Producer thread finished");
    }
}
