package ru.mail.alexander_kurlovich3.task2;

public class Consumer implements Runnable {
    private TaskData data;
    private int chekingTime = 0;
    private final int finalChek = 3;

    public Consumer(TaskData data) {
        this.data = data;
    }

    @Override
    public void run() {
        System.out.println("Consumer thread started");
        Integer someNumber = null;
        while (chekingTime < finalChek){
            someNumber = data.getInt();
            if (someNumber == null ){
                chekingTime ++;
            }
            else {
                System.out.println("Consumer "+ Thread.currentThread().getName() + " number = " + someNumber);
                chekingTime = 0;
            }
        }
        System.out.println("Consumer thread finished");
    }
}
