package ru.mail.alexander_kurlovich3.task2;

import java.util.ArrayDeque;
import java.util.Deque;

public class TaskData {
    private final Deque<Integer> data = new ArrayDeque<>();

    public int getInt (){
        synchronized (data){
            if (data.isEmpty()){
                try {
                    data.wait(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return data.poll();
        }
    }

    public void putInt(int i){
        synchronized (data){
            data.add(i);
            data.notify();
        }
    }
}
