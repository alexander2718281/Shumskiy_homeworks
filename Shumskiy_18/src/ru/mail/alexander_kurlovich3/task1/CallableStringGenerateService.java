package ru.mail.alexander_kurlovich3.task1;

import java.util.Random;
import java.util.concurrent.Callable;

public class CallableStringGenerateService implements Callable<String> {
    private int minNumOfSimbols;
    private int maxNumOfSimbols;
    private char[] simbols;
    private Random random = new Random();

    public CallableStringGenerateService(int minNumOfSimbols, int maxNumOfSimbols, char... simbols) {
        this.minNumOfSimbols = minNumOfSimbols;
        this.maxNumOfSimbols = maxNumOfSimbols;
        this.simbols = simbols;
    }

    @Override
    public String call() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        int numOfSimbols = getRandomIntInRange(minNumOfSimbols, maxNumOfSimbols);
        for (int i = 0; i < numOfSimbols; i++){
            stringBuilder.append(simbols[random.nextInt(simbols.length)]);
        }
        System.out.println(Thread.currentThread().getName() + " " + stringBuilder);
        return stringBuilder.toString();
    }

    private int getRandomIntInRange(int min, int max){
        return random.nextInt(max - min + 1) + min;
    }
}
