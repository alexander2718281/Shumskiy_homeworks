package ru.mail.alexander_kurlovich3.PracticeOnThreadsTask2;

public class ThreadForStartOtherThread extends Thread {
    private int num;
    private Object o1;
    private Object o2;

    public ThreadForStartOtherThread(int num, Object o1) {
        this.num = num;
        this.o1 = o1;
    }

    @Override
    public void run() {
        o2 = new Object();
        synchronized (o1) {
            if (num < 50) {
                Thread thread = new ThreadForStartOtherThread(num + 1, o2);
                thread.start();
                synchronized (o2) {
                    try {
                        o2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hello from thread " + num + " !!!");
            o1.notify();
        }
    }
}
