package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task2.WhileNot0ExepctionService;
import ru.mail.alexander_kurlovich3.task3.MediaBox;

public class Main {

    public static void main(String[] args) {
        // task1
        try {
            Object o = null;
            o.hashCode();
        } catch (NullPointerException exception) {
            System.out.println("Link refers to empty space");
        }

        //task2
        WhileNot0ExepctionService service = new WhileNot0ExepctionService();
        service.circleOfExeptions();


        //task3
        MediaBox mediaBox = new MediaBox();
        mediaBox.increaseVolume();
        mediaBox.increaseVolume();
        mediaBox.increaseVolume();
        mediaBox.printVolume();
    }
}
