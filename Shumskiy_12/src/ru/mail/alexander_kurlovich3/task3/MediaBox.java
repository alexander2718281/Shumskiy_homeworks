package ru.mail.alexander_kurlovich3.task3;

public class MediaBox {
    private Volume volume;

    public MediaBox() {
        this.volume = Volume.LOW;
    }

    public void increaseVolume() {
        int currentVolume = volume.ordinal();
        if (currentVolume < 2) {
            volume = Volume.values()[currentVolume + 1];
        } else {
            System.out.println("The volume is already maximum");
        }
    }

    public void decreaseVolume() {
        int currentVolume = volume.ordinal();
        if (currentVolume > 0) {
            volume = Volume.values()[currentVolume + -1];
        } else {
            System.out.println("The volume is already minimum");
        }
    }

    public void printVolume() {
        System.out.println("Current volume - " + volume);
    }
}



enum Volume{
    LOW,
    AVERAGE,
    HIGH
}
