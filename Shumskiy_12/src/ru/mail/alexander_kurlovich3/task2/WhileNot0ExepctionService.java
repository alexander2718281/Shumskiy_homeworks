package ru.mail.alexander_kurlovich3.task2;

import ru.mail.alexander_kurlovich3.ScanerService;

public class WhileNot0ExepctionService {

    private ScanerService scaner = new ScanerService();

    public void circleOfExeptions() {
        int scan = 31;
        while (scan != 0) {
            System.out.println("Enter number from 0 to 3 (to exit the vicious cycle enter 0)");
            scan = scaner.getIntFromScaner();
            try {
                switch (scan) {
                    case 0:
                        System.out.println("Yoy entered 0!!! The cycle is complete");
                    case 1:
                        throw new ExeptionIfEntered1("You entered 1, will continue");
                    case 2:
                        throw new ExeptionIfEntered2("You entered 2, will continue");
                    case 3:
                        throw new ExeptionIfEntered3("You entered 3, will continue");
                    default:
                        System.out.println("The entered number is outside the requested range");
                }

            } catch (ExeptionIfEntered3 exeptionIfEntered3) {
                System.out.println(exeptionIfEntered3.getMessage());
            } catch (ExeptionIfEntered2 exeptionIfEntered2) {
                System.out.println(exeptionIfEntered2.getMessage());
            } catch (ExeptionIfEntered1 exeptionIfEntered1) {
                System.out.println(exeptionIfEntered1.getMessage());
            }
        }
    }
}
