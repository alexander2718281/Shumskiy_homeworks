package ru.mail.alexander_kurlovich3;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        scanAndNumberOfDigits();
        randomArrayAndPrintEven();
        genereteAndPrinrRandomNumber();
    }

    public static void scanAndNumberOfDigits(){
        System.out.println("Enter the number:");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int summ = 0;
        int i = 0;
        while ((double)num / 10 > 0 ){
            summ = summ + num % 10;
            num /= 10;
            i++;
        }
        System.out.println("Num of numerals: " + i);
        System.out.println("Summ of numerals:" + summ);
    }

    public static void randomArrayAndPrintEven(){
        System.out.println("Enter the size of array:");
        Scanner scanner = new Scanner(System.in);
        int arraySize = scanner.nextInt();
        int[] array = new int[arraySize];
        System.out.println("Enter lower bound of array:");
        int lowerBounnd = scanner.nextInt();
        System.out.println("Enter higher bound of array:");
        int higherBound = scanner.nextInt();
        Random rnd = new Random();
        System.out.println("Random array:");
        for (int i = 0; i < array.length; i++) {
            array[i] = rnd.nextInt(higherBound - lowerBounnd + 1) + lowerBounnd;
            System.out.println(array[i]);
        }
        int numOfEven = 0;
        for (int i : array) {
            if(i % 2 == 0){
                numOfEven++;
            }
        }
        System.out.println("Num of even numbers: " + numOfEven);
    }

    public static void genereteAndPrinrRandomNumber(){
        System.out.println("Enter bounds of a random number:");
        Scanner scanner = new Scanner(System.in);
        int bound = scanner.nextInt();
        Random rnd = new Random();
        int num = rnd.nextInt(2*bound + 1) - bound;
        System.out.println("Random number equally: " + num);
    }
}
