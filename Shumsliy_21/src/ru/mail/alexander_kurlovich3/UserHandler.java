package ru.mail.alexander_kurlovich3;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandler extends DefaultHandler {

    int numOfBooks = 0;
    float sumOfPrices = 0;
    boolean bAuthor = false;
    boolean bTitle = false;
    boolean bGenre = false;
    boolean bPrice = false;
    boolean bDate = false;
    boolean bDescription = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("book")) {
            String id = attributes.getValue("id");
            System.out.println("Book ID: " + id);
            numOfBooks += 1;
        } else if (qName.equalsIgnoreCase("author")) {
            bAuthor = true;
        } else if (qName.equalsIgnoreCase("title")) {
            bTitle = true;
        } else if (qName.equalsIgnoreCase("genre")) {
            bGenre = true;
        } else if (qName.equalsIgnoreCase("price")) {
            bPrice = true;
        } else if (qName.equalsIgnoreCase("publish_date")) {
            bDate = true;
        } else if (qName.equalsIgnoreCase("description")) {
            bDescription = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("book")) {
            System.out.println("End Element :" + qName);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {

        if (bAuthor) {
            System.out.println("Author: " + new String(ch, start, length));
            bAuthor = false;
        } else if (bTitle) {
            System.out.println("Title: " + new String(ch, start, length));
            bTitle = false;
        } else if (bGenre) {
            System.out.println("Genre: " + new String(ch, start, length));
            bGenre = false;
        } else if (bPrice) {
            String price = new String(ch, start, length);
            System.out.println("Price: " + price);
            sumOfPrices += Float.valueOf(price);
            bPrice = false;
        } else if (bDate) {
            System.out.println("Publish date: " + new String(ch, start, length));
            bDate = false;
        } else if (bDescription) {
            System.out.println("Description: " + new String(ch, start, length));
            bDescription = false;
        }
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Num of books = " + numOfBooks);
        System.out.println("Average price = " + sumOfPrices/numOfBooks);
    }
}