package ru.mail.alexander_kurlovich3;

public class ReaaderFactory {
    private static volatile ReaaderFactory reaaderFactory;

    private ReaaderFactory() {

    }

    public synchronized static ReaaderFactory getReaderFactory() {
        if (reaaderFactory == null) {
            synchronized (reaaderFactory) {
                if (reaaderFactory == null) {
                    reaaderFactory = new ReaaderFactory();
                }
            }
        }
        return reaaderFactory;
    }

    public TextReader getTextReader(String addres){
        TextReader reader;
        String format = addres.substring(addres.lastIndexOf('.'), addres.length());
        if (format.equals(".txt")){
            reader = new TxtReader();
        }
        else if(format.equals(".xml")){
            reader = new XmlReader();
        }
        else if (format.equals(".properties")){
            reader = new PropertiesReader();
        }
        else {
            throw new IllegalStateException();
        }
        return reader;
    }
}
