package ru.mail.alexander_kurlovich3;

public interface TextReader {

    String read();
}
