package ru.mail.alexander_kurlovich3;

public class Course {
    private int num1;
    private int num2;
    private int num3;
    private int num4;
    private int num5;

    public Course(int num1, int num2, int num3, int num4, int num5) {
        this.num1 = num1;
        this.num2 = num2;
        this.num3 = num3;
        this.num4 = num4;
        this.num5 = num5;
    }


    private Course (Builder builder){
        this.num1 = builder.num1;
        this.num2 = builder.num2;
        this.num3 = builder.num3;
        this.num4 = builder.num4;
        this.num5 = builder.num5;
    }

    public static Builder newBuilder(){
        return new Builder();
    }

    public static final class Builder {
        private int num1;
        private int num2;
        private int num3;
        private int num4;
        private int num5;

        private Builder() {
        }

        public Builder withNum1(int num){
            this.num1 = num;
            return this;
        }

        public Builder withNum2(int num){
            this.num2 = num;
            return this;
        }

        public Builder withNum3(int num){
            this.num3 = num;
            return this;
        }

        public Builder withNum4(int num){
            this.num4 = num;
            return this;
        }

        public Builder withNum5(int num){
            this.num5 = num;
            return this;
        }

        public Course build(){
            return new Course(this);
        }
    }
}


