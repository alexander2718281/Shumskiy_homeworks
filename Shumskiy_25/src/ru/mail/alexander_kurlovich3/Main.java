package ru.mail.alexander_kurlovich3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File("sqlcommand.txt")))){
            bw.write("CREATE DATABASE Office;");
            bw.newLine();
            bw.write("use Office");
            bw.newLine();
            bw.write("CREATE TABLE Employees (id int(11) NOT NULL PRIMARY KEY, first_name VARCHAR(40), last_name VARCHAR(40), salary INT(11), title_id INT(2));");
            bw.newLine();
            bw.write("CREATE TABLE Title_enum (id INT(2) NOT NULL PRIMARY KEY, title VARCHAR(40));");
            bw.newLine();
            bw.write("INSERT INTO Title_enum VALUES (1, 'JUNIOR');");
            bw.newLine();
            bw.write("INSERT INTO Title_enum VALUES (2, 'MIDDLE');");
            bw.newLine();
            bw.write("INSERT INTO Title_enum VALUES (3, 'SENIOR');");
            bw.newLine();
            Random random = new Random();
            for (int i = 0; i < 10; i++) {
                int salary = random.nextInt(2701) + 300;
                System.out.println(salary);
                bw.write("INSERT INTO Employees VALUES (" + i + ", 'name" + i + "', 'lastName" + i + "', " + salary + ", 1);");
                bw.newLine();
            }
            bw.write("UPDATE Employees SET title_id = 2 WHERE salary >= 1000 AND salary <= 1500;");
            bw.newLine();
            bw.write("UPDATE Employees SET title_id = 3 WHERE salary > 1500;");
            bw.newLine();
            bw.write("DELETE FROM Employees WHERE  salary < 700;");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
