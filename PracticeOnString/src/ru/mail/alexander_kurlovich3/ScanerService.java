package ru.mail.alexander_kurlovich3;

import java.util.Scanner;

public class ScanerService {
    private Scanner scanner = new Scanner(System.in);

    public String getStringFromScanner(){
        String line = scanner.nextLine();
        return line;
    }
}
