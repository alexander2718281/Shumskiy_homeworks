package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.ReverseService;
import ru.mail.alexander_kurlovich3.task2.DeleteShortWordsService;
import ru.mail.alexander_kurlovich3.task3.RedactingOfStringArrayService;
import ru.mail.alexander_kurlovich3.task4.SpaceAddService;

public class Main {

    public static void main(String[] args) {
        ScanerService scaner = new ScanerService();
        //System.out.println("Enter some line:");
        //System.out.println(ReverseService.reverseOrderOfWords(scaner.getStringFromScanner()));

        //System.out.println("Enter some line:");
        //System.out.println(DeleteShortWordsService.deletShortWords(scaner.getStringFromScanner()));

//        String[] words = new String[3];
//        for (int i = 0; i < words.length; i++){
//            words[i] = scaner.getStringFromScanner();
//        }
//        String[] lines = RedactingOfStringArrayService.redactStringArray(5,words);
//        for (String line : lines) {
//            System.out.println(line);
//        }

        System.out.println("Enter long line:");
        System.out.println(SpaceAddService.addSpaceAfterPunktuation(scaner.getStringFromScanner()));
    }
}
