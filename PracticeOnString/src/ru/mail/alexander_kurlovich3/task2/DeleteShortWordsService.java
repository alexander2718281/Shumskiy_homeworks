package ru.mail.alexander_kurlovich3.task2;

public class DeleteShortWordsService {
    public static String deletShortWords(String startString){
        String[] words = startString.split(" ");
        String finalString = new String();
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() >= 5){
                finalString = finalString.concat(words[i]).concat(" ");
            }
        }
        return finalString;
    }
}
