package ru.mail.alexander_kurlovich3.task1;

public class ReverseService {

    public static String reverseOrderOfWords(String startString){
        String[] words = startString.split(" ");
        String finalString = new String();
        for (int i = words.length - 1; i >= 0; i--) {
            finalString = finalString.concat(" ").concat(words[i]);
        }
        finalString = finalString.trim();
        return finalString;
    }
}
