package ru.mail.alexander_kurlovich3.task4;

public class SpaceAddService {

    public static String addSpaceAfterPunktuation(String line){
        String[] parts = line.split("[.,!?]\\S");
        String finalString = new String();
        line = line.concat(" ");
        for (int i = 0; i < parts.length; i++) {
            if(i>0){
                finalString = finalString.concat(line.substring(finalString.length()-i, finalString.length() -i + parts[i].length()+2)).concat(" ");
            }
            else {finalString = finalString.concat(line.substring(finalString.length()-i, finalString.length() -i + parts[i].length()+1)).concat(" ");
            }
        }
        finalString = finalString.trim();
        return finalString;
    }
}
