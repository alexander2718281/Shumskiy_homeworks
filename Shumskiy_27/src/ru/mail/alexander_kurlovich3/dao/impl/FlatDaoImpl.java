package ru.mail.alexander_kurlovich3.dao.impl;

import ru.mail.alexander_kurlovich3.dao.FlatDao;
import ru.mail.alexander_kurlovich3.dao.connection.ConnectionService;
import ru.mail.alexander_kurlovich3.dao.model.Flat;
import ru.mail.alexander_kurlovich3.dao.model.House;

import java.io.File;
import java.sql.*;

public class FlatDaoImpl implements FlatDao {
    @Override
    public void createTable() {
        String createTableSQL = "CREATE TABLE IF NOT EXISTS db_flat("
                + "id INTEGER (11) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                + "size INTEGER(4) NOT NULL, "
                + "countOfRooms INTEGER(2) NOT NULL, "
                + "floor INTEGER(3) NOT NULL, "
                + "isPopulated BOOLEAN NOT NULL, "
                + "house_id INTEGER(11) NOT NULL"
                + ")";
        execute(createTableSQL);
    }

    @Override
    public void dropTable() {
        String dropTableSQL = "DROP TABLE IF EXISTS db_flat";
        execute(dropTableSQL);
    }

    @Override
    public void save(Flat flat) {
        String saveSql = "INSERT INTO db_flat(size, countOfRooms, floor, isPopulated, house_id) VALUES(?, ?, ?, ?, ?);";
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(saveSql)) {
                preparedStatement.setInt(1, Integer.valueOf(flat.getSize()));
                preparedStatement.setInt(2, flat.getCountOfRomms());
                preparedStatement.setInt(3, flat.getFloor());
                preparedStatement.setBoolean(4, flat.isPopulated());
                preparedStatement.setLong(5, flat.getHouse().getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public Flat getById(long id) {
        String selectById = "SELECT db_flat.id, size, countOfRooms, floor, isPopulated, house_id, address FROM db_flat JOIN db_house ON db_flat.house_id = db_house.id WHERE db_flat.id = ?;";
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectById)) {
                preparedStatement.setLong(1, id);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (rs.next()) {
                        House house = new House(rs.getLong("house_id"), rs.getString("address"));
                        Flat flat = Flat.newBuilder().withId(rs.getLong("db_flat.id"))
                                .withSize(String.valueOf(rs.getInt("size")))
                                .withCountOfRooms(rs.getInt("countOfRooms"))
                                .withFloor(rs.getInt("floor"))
                                .withIsPopuleted(rs.getBoolean("isPopulated"))
                                .withHouse(house)
                                .build();
                        return flat;
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    @Override
    public int occupyFlatsWithSomeNumberOfRomms(int numOfRooms, int maxSize) {
        String updateSql = "UPDATE db_flat SET isPopulated = true WHERE countOfRooms = ? AND size < ?;";
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateSql)) {
                preparedStatement.setInt(1, numOfRooms);
                preparedStatement.setInt(2, maxSize);
                int countOfOccupiedFlats = preparedStatement.executeUpdate();
                return countOfOccupiedFlats;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Override
    public void printAllOccupiedFlats() {
        String selectSql = "SELECT db_flat.id, size, countOfRooms, floor, address FROM db_flat JOIN db_house ON db_flat.house_id = db_house.id WHERE isPopulated = true;";
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSql)) {
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    int countOfFlats = 0;
                    while (rs.next()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Flat " + rs.getLong("db_flat.id"))
                                .append(", size - " + rs.getInt("size"))
                                .append(", count of rooms - " + rs.getInt("countOfRooms"))
                                .append(", floor - " + rs.getInt("floor"))
                                .append(", occupied")
                                .append(", address - " + rs.getString("address"));
                        System.out.println(sb.toString());
                        countOfFlats += 1;
                    }
                    System.out.println("Count of occupied flats = " + countOfFlats);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(String sql) {
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(sql);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }
}
