package ru.mail.alexander_kurlovich3.dao.impl;

import ru.mail.alexander_kurlovich3.dao.HouseDao;
import ru.mail.alexander_kurlovich3.dao.connection.ConnectionService;
import ru.mail.alexander_kurlovich3.dao.model.House;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HouseDaoImpl implements HouseDao {
    @Override
    public void createTable() {
        String createTableSQL = "CREATE TABLE IF NOT EXISTS db_house("
                + "id INTEGER (11) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                + "address VARCHAR(100) NOT NULL"
                + ")";
        execute(createTableSQL);
    }

    @Override
    public void dropTable() {
        String dropTableSQL = "DROP TABLE IF EXISTS db_house";
        execute(dropTableSQL);
    }

    @Override
    public void save(House house) {
        String sql = "INSERT INTO db_house(address) VALUES(?);";
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, house.getAddress());
                preparedStatement.executeUpdate();
                try(ResultSet rs = preparedStatement.getGeneratedKeys()){
                    if (rs.next()){
                        house.setId(rs.getLong(1));
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<House> getHouses() {
        String selectSql = "SELECT * FROM db_house;";
        List<House> houses = new ArrayList<>();
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSql)) {
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {
                        houses.add(new House(rs.getLong("id"), rs.getString("address")));
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
        return houses;
    }

    private void execute(String sql) {
        Connection connection = ConnectionService.getConnection();
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(sql);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }
}
