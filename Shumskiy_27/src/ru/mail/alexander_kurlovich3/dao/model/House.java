package ru.mail.alexander_kurlovich3.dao.model;

public class House {

    private long id;
    private String address;

    public House(String address) {
        this.address = address;
    }

    public House(long id, String address) {
        this.id = id;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("House{");
        sb.append("id=").append(id);
        sb.append(", address='").append(address).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public void setId(long id) {
        this.id = id;
    }
}
