package ru.mail.alexander_kurlovich3.dao.model;

public class Flat {
    private long id;
    private String size;
    private int countOfRomms;
    private int floor;
    private boolean isPopulated;
    private House house;

    public Flat(String size, int countOfRomms, int floor, boolean isPopulated, House house) {
        this.size = size;
        this.countOfRomms = countOfRomms;
        this.floor = floor;
        this.isPopulated = isPopulated;
        this.house = house;
    }

    public Flat(long id, String size, int countOfRomms, int floor, boolean isPopulated, House house) {
        this.id = id;
        this.size = size;
        this.countOfRomms = countOfRomms;
        this.floor = floor;
        this.isPopulated = isPopulated;
        this.house = house;
    }

    public long getId() {
        return id;
    }

    public String getSize() {
        return size;
    }

    public int getCountOfRomms() {
        return countOfRomms;
    }

    public int getFloor() {
        return floor;
    }

    public boolean isPopulated() {
        return isPopulated;
    }

    public House getHouse() {
        return house;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Flat{");
        sb.append("id=").append(id);
        sb.append(", size='").append(size).append('\'');
        sb.append(", countOfRomms=").append(countOfRomms);
        sb.append(", floor=").append(floor);
        sb.append(", isPopulated=").append(isPopulated);
        sb.append(", house=").append(house);
        sb.append('}');
        return sb.toString();
    }

    public static FlatBuilder newBuilder(){
        return new FlatBuilder();
    }

    private Flat(FlatBuilder builder){
        id = builder.id;
        size = builder.size;
        countOfRomms = builder.countOfRomms;
        floor = builder.floor;
        isPopulated = builder.isPopulated;
        house = builder.house;
    }

    public static final class FlatBuilder{
        private long id;
        private String size;
        private int countOfRomms;
        private int floor;
        private boolean isPopulated;
        private House house;

        public FlatBuilder withId(long id){
            this.id = id;
            return this;
        }

        public FlatBuilder withSize(String size){
            this.size = size;
            return this;
        }

        public FlatBuilder withCountOfRooms(int countOfRomms){
            this.countOfRomms = countOfRomms;
            return this;
        }

        public FlatBuilder withFloor(int floor){
            this.floor = floor;
            return this;
        }

        public FlatBuilder withIsPopuleted(boolean isPopulated){
            this.isPopulated = isPopulated;
            return this;
        }

        public FlatBuilder withHouse(House house){
            this.house = house;
            return this;
        }

        public Flat build(){
            return new Flat(this);
        }

    }
}
