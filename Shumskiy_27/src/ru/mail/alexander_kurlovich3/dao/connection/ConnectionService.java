package ru.mail.alexander_kurlovich3.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {
    private static Connection connection;

    private ConnectionService(){}

    public static Connection getConnection(){
        if (connection == null){
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Where is your MySQL JDBC Driver?");
                e.printStackTrace();
                return null;
            }
            try {
                connection =  DriverManager.getConnection("jdbc:mysql://localhost:3306/city?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "1234");
            } catch (SQLException e) {
                System.out.println("Connection Failed! Check output console");
                e.printStackTrace();
                return null;
            }

        }
        return connection;
    }
}
