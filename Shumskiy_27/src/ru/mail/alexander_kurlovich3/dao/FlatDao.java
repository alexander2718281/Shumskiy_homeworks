package ru.mail.alexander_kurlovich3.dao;

import ru.mail.alexander_kurlovich3.dao.model.Flat;

public interface FlatDao {

    void createTable();
    void dropTable();
    void save(Flat flat);
    Flat getById(long id);
    int occupyFlatsWithSomeNumberOfRomms(int numOfRooms, int maxSize);
    void printAllOccupiedFlats();
}
