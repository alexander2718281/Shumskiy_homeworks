package ru.mail.alexander_kurlovich3.dao;

import ru.mail.alexander_kurlovich3.dao.model.House;

import java.util.List;

public interface HouseDao {

    void createTable();
    void dropTable();
    void save(House house);
    List<House> getHouses();
}
