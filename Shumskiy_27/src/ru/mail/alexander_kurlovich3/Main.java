package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.dao.FlatDao;
import ru.mail.alexander_kurlovich3.dao.HouseDao;
import ru.mail.alexander_kurlovich3.dao.impl.FlatDaoImpl;
import ru.mail.alexander_kurlovich3.dao.impl.HouseDaoImpl;
import ru.mail.alexander_kurlovich3.dao.model.Flat;
import ru.mail.alexander_kurlovich3.dao.model.House;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        HouseDao houseDao = new HouseDaoImpl();
        FlatDao flatDao = new FlatDaoImpl();
        houseDao.dropTable();
        houseDao.createTable();
        flatDao.dropTable();
        flatDao.createTable();
        List[] lists = GeneretorService.getInstance().generateHousesAndFlats(3, 20);
        for (Object o : lists[0]) {
            houseDao.save((House) o);
        }
        for (Object o : lists[1]) {
            flatDao.save((Flat) o);
        }
        flatDao.occupyFlatsWithSomeNumberOfRomms(1,50);
        flatDao.printAllOccupiedFlats();
    }
}
