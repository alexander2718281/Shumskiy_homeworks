package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.dao.model.Flat;
import ru.mail.alexander_kurlovich3.dao.model.House;

import java.util.ArrayList;
import java.util.List;

public class GeneretorService {
    private static GeneretorService service;

    private GeneretorService() {
    }

    public static GeneretorService getInstance() {
        if (service == null) {
            service = new GeneretorService();
        }
        return service;
    }

    public List[] generateHousesAndFlats(int numOfHouese, int flatsInAHouse) {
        List[] lists = new List[2];
        lists[0] = new ArrayList();
        lists[1] = new ArrayList();
        Flat.FlatBuilder builder = Flat.newBuilder();
        RandomService randomService = RandomService.getInstance();
        for (int i = 0; i < numOfHouese; i++) {
            House house = new House("Address" + i);
            lists[0].add(house);
            for (int j = 0; j < flatsInAHouse; j++) {
                lists[1].add(builder.withSize(String.valueOf(randomService.getRandom(40, 80)))
                        .withCountOfRooms(randomService.getRandom(1, 3))
                        .withFloor(randomService.getRandom(1, 5))
                        .withIsPopuleted(false)
                        .withHouse(house)
                        .build());
            }
        }
        return lists;
    }
}
