package ru.mail.alexander_kurlovich3;

import java.util.Random;

public class RandomService {

    private static RandomService service;
    private Random random = new Random();

    private RandomService(){}

    public static RandomService getInstance(){
        if (service == null){
            service = new RandomService();
        }
        return service;
    }

    public int getRandom(int minValue, int maxValue){
        return random.nextInt(maxValue - minValue + 1) + minValue;
    }
}
