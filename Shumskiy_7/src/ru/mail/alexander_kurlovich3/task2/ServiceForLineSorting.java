package ru.mail.alexander_kurlovich3.task2;

public class ServiceForLineSorting {

    public static void sortLine(String... lines){
        int numOfElseLines = 0;
        for (int i = 0; i < lines.length; i++) {
            if(hasWords(lines[i], "cat", "dog")){
                System.out.println(lines[i]);
                System.out.println("Length of line: " + lines[i].length());
            }
            else {
                numOfElseLines ++;
            }
        }
        System.out.println(numOfElseLines + " lines don't have words cat and dog");
    }

    private static boolean hasWords(String str, String... words){
        for (String word : words) {
            if(str.indexOf(word) != -1){
                return true;
            }
        }
        return false;
    }
}
