package ru.mail.alexander_kurlovich3;

import java.util.Scanner;

public class ScanerService {
    private Scanner scanner = new Scanner(System.in);

    public String getStringFromScaner(){
        String line = scanner.nextLine();
        return line;
    }

    public int getIntFromScanner(){
        int num = scanner.nextInt();
        return num;
    }

}
