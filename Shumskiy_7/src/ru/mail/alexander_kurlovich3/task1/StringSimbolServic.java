package ru.mail.alexander_kurlovich3.task1;

public class StringSimbolServic {

    public static void findSimbolsLikeLastSimbol(String str){
        System.out.println("Symbols coincide last symbol:");
        char[] ch = str.toCharArray();
        for (int i = 0; i < ch.length - 1; i++) {
            if(ch[i] == ch[ch.length-1]){
                System.out.println(i);
            }
        }
    }
}
