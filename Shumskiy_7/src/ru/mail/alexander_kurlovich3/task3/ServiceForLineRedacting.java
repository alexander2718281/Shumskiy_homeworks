package ru.mail.alexander_kurlovich3.task3;

public class ServiceForLineRedacting {

    public static String deleteSpaceBetweenQuestionMark2and3(String str){
        int numOfFirstQuestionMark = str.indexOf('?');
        int numOfSecondQuestionMark = str.indexOf('?', numOfFirstQuestionMark+1);
        int numOfTheerdQuestionMark = str.indexOf('?', numOfSecondQuestionMark+1);
        String partBetweenSpaces = str.substring(numOfSecondQuestionMark, numOfTheerdQuestionMark);
        while (partBetweenSpaces.indexOf(' ') != -1){
            partBetweenSpaces = partBetweenSpaces.substring(0, partBetweenSpaces.indexOf(' ')).concat(partBetweenSpaces.substring(partBetweenSpaces.indexOf(' ')+1, partBetweenSpaces.length()));
        }
        String finalString = str.substring(0, numOfSecondQuestionMark).concat(partBetweenSpaces).concat(str.substring(numOfTheerdQuestionMark, str.length()));
        return finalString;
    }

}
