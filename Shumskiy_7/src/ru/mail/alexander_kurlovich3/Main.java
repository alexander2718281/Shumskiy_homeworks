package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.StringSimbolServic;
import ru.mail.alexander_kurlovich3.task2.ServiceForLineSorting;
import ru.mail.alexander_kurlovich3.task3.ServiceForLineRedacting;


public class Main {

    public static void main(String[] args) {
        ScanerService scaner = new ScanerService();
        System.out.println("Enter some line:");
        StringSimbolServic.findSimbolsLikeLastSimbol(scaner.getStringFromScaner());
        System.out.println("Enter number of lines:");
        int numOfLines = scaner.getIntFromScanner();
        String[] lines = new String[numOfLines];
        scaner.getStringFromScaner();
        System.out.println("Enter lines:");
        for(int i = 0; i < numOfLines; i++){
            lines[i] = scaner.getStringFromScaner();
        }
        ServiceForLineSorting.sortLine(lines);
        System.out.println("Enter some line:");
        String string = ServiceForLineRedacting.deleteSpaceBetweenQuestionMark2and3(scaner.getStringFromScaner());
        System.out.println(string);
    }

}
