package ru.mail.alexander_kurlovich3.task1;

import java.util.List;

public class ListIntSortService {
    public int[] findTwoMaxInt(List<Integer> listOfIntegers) {
        int max;
        int premax;
        if (listOfIntegers.get(0) > listOfIntegers.get(1)) {
            max = listOfIntegers.get(0);
            premax = listOfIntegers.get(1);
        } else {
            max = listOfIntegers.get(1);
            premax = listOfIntegers.get(0);
        }
        for (int i = 2; i < listOfIntegers.size(); i++) {
            if (listOfIntegers.get(i) >= max) {
                premax = max;
                max = listOfIntegers.get(i);
            } else if (listOfIntegers.get(i) > premax) {
                premax = listOfIntegers.get(i);
            }
        }
        int[] array = {max, premax};
        return array;
    }
}
