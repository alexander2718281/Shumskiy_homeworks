package ru.mail.alexander_kurlovich3.task2;

import java.util.ArrayList;
import java.util.List;

public class StringChengeService {
    public List<String> changePlusInStrings(List<String> lines){
        List<String> changedLines = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            changedLines.add(lines.get(i).replaceAll("[+]", "+++"));
        }
        return changedLines;
    }
}
