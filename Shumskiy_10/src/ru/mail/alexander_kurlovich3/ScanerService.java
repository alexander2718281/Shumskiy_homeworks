package ru.mail.alexander_kurlovich3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScanerService {
    private Scanner scanner = new Scanner(System.in);

    public String getString() {
        String str = scanner.nextLine();
        return str;
    }

    public List<String> getStringListFromScanner(int numOfLines) {
        List<String> lines = new ArrayList<>(numOfLines);
        for (int i = 0; i < numOfLines; i++) {
            System.out.println("Enter some line:");
            lines.add(getString());
        }
        return lines;
    }
}
