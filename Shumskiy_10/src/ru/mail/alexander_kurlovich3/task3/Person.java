package ru.mail.alexander_kurlovich3.task3;

public class Person {
    private String name;
    private String surename;
    private int age;

    public Person(String name, String surename, int age) {
        this.name = name;
        this.surename = surename;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurename() {
        return surename;
    }

    public int getAge() {
        return age;
    }

    public void getInfo() {
        if (age >= 18) {
            System.out.println("Adult: " + name + " " + surename + " " + age);
        } else {
            System.out.println("Infant: " + name + " " + surename + " " + age);
        }
    }
}
