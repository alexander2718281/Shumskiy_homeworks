package ru.mail.alexander_kurlovich3;

import java.util.Random;

public class Randomazer {
    private Random random = new Random();

    public int randomIntValueInTheRange(int from, int to) {
        int start, end;
        if (from >= to) {
            end = from;
            start = to;
        } else {
            start = from;
            end = to;
        }
        return random.nextInt(end - start + 1) + start;
    }
}
