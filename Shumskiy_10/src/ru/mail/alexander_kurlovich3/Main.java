package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.ListIntSortService;
import ru.mail.alexander_kurlovich3.task2.StringChengeService;
import ru.mail.alexander_kurlovich3.task3.Person;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //task1
        Randomazer random = new Randomazer();

        int numOfIntegers = 10;
        List<Integer> listOfIntegers = new ArrayList<Integer>(numOfIntegers);
        for (int i = 0; i < numOfIntegers; i++) {
            listOfIntegers.add(random.randomIntValueInTheRange(2, 98));
        }
        ListIntSortService serviceTask1 = new ListIntSortService();
        int[] maxValues = serviceTask1.findTwoMaxInt(listOfIntegers);
        System.out.printf("Max value = %d", maxValues[0]);
        System.out.printf("\nSecond max value = %d", maxValues[1]);

        //task2
        ScanerService scaner = new ScanerService();

        List<String> scanerLines = scaner.getStringListFromScanner(5);
        StringChengeService serviseTask2 = new StringChengeService();
        scanerLines = serviseTask2.changePlusInStrings(scanerLines);
        System.out.println(scanerLines);


        //task3
        int numOfPersons = 20;
        List<Person> persons = new ArrayList<Person>(numOfPersons);
        for (int i = 0; i < numOfPersons; i++) {
            System.out.println("Enter name and then surename");
            persons.add(new Person(scaner.getString(), scaner.getString(), random.randomIntValueInTheRange(15, 30)));
        }
        for (Person person : persons) {
            person.getInfo();
        }

    }
}
