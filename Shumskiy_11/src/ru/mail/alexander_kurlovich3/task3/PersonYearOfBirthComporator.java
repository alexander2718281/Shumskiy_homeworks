package ru.mail.alexander_kurlovich3.task3;

import java.util.Comparator;

public class PersonYearOfBirthComporator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        if (o1.getYearOfBirth() > o2.getYearOfBirth()) {
            return 1;
        } else if (o1.getYearOfBirth() < o2.getYearOfBirth()) {
            return -1;
        } else {
            return 0;
        }
    }
}
