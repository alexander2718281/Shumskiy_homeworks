package ru.mail.alexander_kurlovich3.task3;

import java.util.Objects;

public class Person {

    private int yearOfBirth;
    private String name;
    private String surename;

    public Person(int yearOfBirth, String name, String surename) {
        this.yearOfBirth = yearOfBirth;
        this.name = name;
        this.surename = surename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return yearOfBirth == person.yearOfBirth &&
                Objects.equals(name, person.name) &&
                Objects.equals(surename, person.surename);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yearOfBirth, name, surename);
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getSurename() {
        return surename;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Person{");
        sb.append("yearOfBirth=").append(yearOfBirth);
        sb.append(", surename='").append(surename).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
