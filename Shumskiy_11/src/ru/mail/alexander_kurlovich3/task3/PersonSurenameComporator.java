package ru.mail.alexander_kurlovich3.task3;

import java.util.Comparator;

public class PersonSurenameComporator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getSurename().compareTo(o2.getSurename());
    }
}
