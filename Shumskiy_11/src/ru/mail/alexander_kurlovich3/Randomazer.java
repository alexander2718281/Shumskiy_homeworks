package ru.mail.alexander_kurlovich3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Randomazer {
    private Random random = new Random();

    public int getRandomInt(int maxValue) {
        return random.nextInt(maxValue + 1);
    }


    public int getRandomInt(int minValue, int maxValue) {
        int start, end;
        if (maxValue >= minValue) {
            start = minValue;
            end = maxValue;
        } else {
            start = maxValue;
            end = minValue;
        }
        return random.nextInt(end - start + 1) + start;
    }

    public List<Integer> getRandomListOfInt(int numOfElements, int maxValue) {
        List<Integer> someNumbers = new ArrayList<>(numOfElements);
        for (int i = 0; i < numOfElements; i++) {
            someNumbers.add(getRandomInt(maxValue));
        }
        return someNumbers;
    }
}
