package ru.mail.alexander_kurlovich3;

import java.util.Scanner;

public class ScanerService {
    private Scanner scanner = new Scanner(System.in);

    public String getStringFromScaner() {
        return scanner.nextLine();
    }

    public int getIntFromScaner(){
        return scanner.nextInt();
    }
}
