package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.IntegerSortInMapService;
import ru.mail.alexander_kurlovich3.task2.PrintFromMapService;
import ru.mail.alexander_kurlovich3.task3.Person;
import ru.mail.alexander_kurlovich3.task3.PersonNameComporator;
import ru.mail.alexander_kurlovich3.task3.PersonSurenameComporator;
import ru.mail.alexander_kurlovich3.task3.PersonYearOfBirthComporator;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        //task1
        Randomazer random = new Randomazer();
        IntegerSortInMapService mapService = new IntegerSortInMapService();
        Map<Integer, Integer> mapOfIntegers = mapService.calculationOfIdenticalValues(random.getRandomListOfInt(50, 20));
        Set<Map.Entry<Integer, Integer>> setOfEntryes = mapOfIntegers.entrySet();
        for (Map.Entry<Integer, Integer> setOfEntrye : setOfEntryes) {
            System.out.println("Number of " + setOfEntrye.getKey() + " = " + setOfEntrye.getValue());
        }

        //task2
        ScanerService scaner = new ScanerService();
        int numOfStringElements = 10;
        String key, value;
        Map<String, String> mapOfStrings = new HashMap<>();
        for(int i = 0; i < numOfStringElements; i++){
            System.out.println("Enter key:");
            key = scaner.getStringFromScaner();
            System.out.println("Enter value:");
            value = scaner.getStringFromScaner();
            mapOfStrings.put(key, value);
        }
        PrintFromMapService printFromMapService = new PrintFromMapService();
        printFromMapService.printKeysOfMap(mapOfStrings);

        //task3
        int numOfPerson = 10;
        Comparator<Person> comparator = new PersonYearOfBirthComporator().thenComparing(new PersonSurenameComporator()).thenComparing(new PersonNameComporator());
        Set<Person> setOfPersons = new TreeSet<>(comparator);
        for (int i = 0; i < numOfPerson; i++){
            System.out.println("Enter year of birth:");
            int yearOfBirth = scaner.getIntFromScaner();
            scaner.getStringFromScaner();
            System.out.println("Enter surename:");
            String surename = scaner.getStringFromScaner();
            System.out.println("Enter name:");
            String name = scaner.getStringFromScaner();
            setOfPersons.add(new Person(yearOfBirth, name, surename));
        }
        for (Person setOfPerson : setOfPersons) {
            System.out.println(setOfPerson.toString());
        }
    }
}
