package ru.mail.alexander_kurlovich3.task1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntegerSortInMapService {

    public Map<Integer, Integer> calculationOfIdenticalValues(List<Integer> list) {
        Map<Integer, Integer> mapOfDiferentIntegers = new HashMap<>();
        for (Integer integer : list) {
            if (mapOfDiferentIntegers.containsKey(integer)) {
                int numOfIntegers = mapOfDiferentIntegers.get(integer) + 1;
                mapOfDiferentIntegers.put(integer, numOfIntegers);
            } else {
                mapOfDiferentIntegers.put(integer, 1);
            }
        }
        return mapOfDiferentIntegers;
    }

}
