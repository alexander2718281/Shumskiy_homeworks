package ru.mail.alexander_kurlovich3.task2;

import java.util.Map;
import java.util.Set;

public class PrintFromMapService {
    public void printKeysOfMap(Map<String, String> someMap){
        Set<String> keySet = someMap.keySet();
        for (String o : keySet) {
            System.out.println(o.toString());
        }
    }
}
