package ru.mail.alexander_kurlovich3.task_2;

public class EnglandAutomobile extends Automobile {
    @Override
    public int getRate() {
        return 3;
    }

    @Override
    public String gerDescription() {
        String str = super.gerDescription().concat("Моя страна - Англия. Рейтинг автомобиля "+ getRate());
        return str;
    }
}
