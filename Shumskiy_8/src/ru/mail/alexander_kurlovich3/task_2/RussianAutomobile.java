package ru.mail.alexander_kurlovich3.task_2;

public class RussianAutomobile extends Automobile {
    @Override
    public int getRate() {
        return 5;
    }

    @Override
    public String gerDescription() {
        String str = super.gerDescription().concat("Моя страна - Россия. Рейтинг автомобиля "+ getRate());
        return str;
    }
}
