package ru.mail.alexander_kurlovich3.task_2;

public class GermanyAutomobile extends Automobile {
    @Override
    public int getRate() {
        return 4;
    }

    @Override
    public String gerDescription() {
        String str = super.gerDescription().concat("Моя страна - Германия. Рейтинг автомобиля "+ getRate());
        return str;
    }
}
