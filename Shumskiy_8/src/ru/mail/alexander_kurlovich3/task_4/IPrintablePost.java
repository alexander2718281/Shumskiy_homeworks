package ru.mail.alexander_kurlovich3.task_4;

public interface IPrintablePost {
    default void printPost(){
        System.out.println("Post is " + this.getClass().getSimpleName());
    }
}
