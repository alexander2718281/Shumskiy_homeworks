package ru.mail.alexander_kurlovich3.task_3;

import java.math.BigDecimal;

public class EmployeeWithHourlyPayment extends Employee {

    private double hourlyPayment;
    private int houres;

    public EmployeeWithHourlyPayment(String name, String surename, String department, String position, double hourlyPayment, int houres) {
        super(name, surename, department, position);
        this.hourlyPayment = hourlyPayment;
        this.houres = houres;
    }

    @Override
    public BigDecimal calculateSalary() {
        BigDecimal salary = BigDecimal.valueOf(hourlyPayment).multiply(BigDecimal.valueOf(houres));
        return salary;
    }
}
