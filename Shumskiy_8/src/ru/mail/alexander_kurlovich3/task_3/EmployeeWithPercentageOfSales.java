package ru.mail.alexander_kurlovich3.task_3;

import java.math.BigDecimal;

public class EmployeeWithPercentageOfSales extends Employee {

    private double percentageOfSales;
    private double salesAmount;

    public EmployeeWithPercentageOfSales(String name, String surename, String department, String position, double percentageOfSales, double salesAmount) {
        super(name, surename, department, position);
        this.percentageOfSales = percentageOfSales;
        this.salesAmount = salesAmount;
    }

    @Override
    public BigDecimal calculateSalary() {
        BigDecimal selery = BigDecimal.valueOf(percentageOfSales).multiply(BigDecimal.valueOf(salesAmount));
        return selery;
    }
}
