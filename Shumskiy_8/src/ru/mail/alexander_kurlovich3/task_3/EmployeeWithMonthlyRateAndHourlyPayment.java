package ru.mail.alexander_kurlovich3.task_3;

import java.math.BigDecimal;

public class EmployeeWithMonthlyRateAndHourlyPayment extends Employee {

    private double monthlyRate;
    private double percentageOfSales;
    private double salesAmount;

    public EmployeeWithMonthlyRateAndHourlyPayment(String name, String surename, String department, String position, double monthlyRate, double percentageOfSales, double salesAmount) {
        super(name, surename, department, position);
        this.monthlyRate = monthlyRate;
        this.percentageOfSales = percentageOfSales;
        this.salesAmount = salesAmount;
    }

    @Override
    public BigDecimal calculateSalary() {
        BigDecimal salary = BigDecimal.valueOf(percentageOfSales).multiply(BigDecimal.valueOf(salesAmount)).add(BigDecimal.valueOf(monthlyRate));
        return calculateSalary();
    }
}
