package ru.mail.alexander_kurlovich3.task_3;

import java.math.BigDecimal;

public abstract class Employee {
    private String name;
    private String surename;
    private String department;
    private String position;

    public Employee(String name, String surename, String department, String position) {
        this.name = name;
        this.surename = surename;
        this.department = department;
        this.position = position;
    }

    public abstract BigDecimal calculateSalary();
}
