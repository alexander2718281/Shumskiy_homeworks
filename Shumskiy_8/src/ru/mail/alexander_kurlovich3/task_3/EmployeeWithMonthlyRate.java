package ru.mail.alexander_kurlovich3.task_3;

import java.math.BigDecimal;

public class EmployeeWithMonthlyRate extends Employee {

    private double monthlyRate;

    public EmployeeWithMonthlyRate(String name, String surename, String department, String position, double monthlyRate) {
        super(name, surename, department, position);
        this.monthlyRate = monthlyRate;
    }

    @Override
    public BigDecimal calculateSalary() {
        BigDecimal salary = BigDecimal.valueOf(monthlyRate);
        return salary;
    }
}
