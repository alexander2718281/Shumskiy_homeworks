package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task_1.Circle;
import ru.mail.alexander_kurlovich3.task_1.Figure;
import ru.mail.alexander_kurlovich3.task_1.Ractangle;
import ru.mail.alexander_kurlovich3.task_1.Triangle;
import ru.mail.alexander_kurlovich3.task_4.Accountant;
import ru.mail.alexander_kurlovich3.task_4.Director;
import ru.mail.alexander_kurlovich3.task_4.IPrintablePost;
import ru.mail.alexander_kurlovich3.task_4.Worker;

public class Main {

    public static void main(String[] args) {

        //Task_1
        Figure[] figures = new Figure[5];
        figures[0] = new Ractangle(10, 20);
        figures[1] = new Triangle(3, 4, 5);
        figures[2] = new Circle(5);
        figures[3] = new Triangle(4, 5, 7);
        figures[4] = new Ractangle(6, 3);
        double summOfPerimeters = 0;
        for (Figure figure : figures) {
            summOfPerimeters += figure.getPerimeter();
            System.out.println(summOfPerimeters);
        }


        //Task_4
        IPrintablePost[] arrayOfPosts = new IPrintablePost[3];
        arrayOfPosts[0] = new Director();
        arrayOfPosts[1] = new Accountant();
        arrayOfPosts[2] = new Worker();
        for (IPrintablePost post : arrayOfPosts) {
            post.printPost();
        }
    }
}
