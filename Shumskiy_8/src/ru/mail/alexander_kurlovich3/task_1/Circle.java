package ru.mail.alexander_kurlovich3.task_1;

public class Circle extends Figure {

    private double radius;

    public Circle(double radius) {
        if (radius > 0) {
            this.radius = radius;
        } else {
            System.out.println("Invalid value");
        }
    }

    @Override
    public double getArea() {
        double area = Math.PI*radius*radius;
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimetr = 2*Math.PI*radius;
        return perimetr;
    }
}
