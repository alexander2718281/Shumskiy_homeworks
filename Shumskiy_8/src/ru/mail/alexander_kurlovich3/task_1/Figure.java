package ru.mail.alexander_kurlovich3.task_1;

public abstract class Figure {

    public abstract double getArea();
    public abstract double getPerimeter();

}
