package ru.mail.alexander_kurlovich3.task_1;

public class Triangle extends Figure {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        if (sideA > 0 && sideB > 0 && sideC > 0) {
            this.sideA = sideA;
            this.sideB = sideB;
            this.sideC = sideC;
        } else {
            System.out.println("Invalid value");
        }
    }

    @Override
    public double getArea() {
        double halfPerimert = getPerimeter()/2;
        double area = Math.sqrt(halfPerimert*(halfPerimert-sideA)*(halfPerimert-sideB)*(halfPerimert-sideC));
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimetr = sideA + sideB + sideC;
        return perimetr;
    }
}
