package ru.mail.alexander_kurlovich3.task_1;

public class Ractangle extends Figure {

    private double length;
    private double width;

    public Ractangle(double length, double width) {
        if (length > 0 && width > 0) {
            this.length = length;
            this.width = width;
        } else {
            System.out.println("Invalid value");
        }
    }

    @Override
    public double getArea() {
        double area = length*width;
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimetr = 2*length + 2* width;
        return perimetr;
    }
}
