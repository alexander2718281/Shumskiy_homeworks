package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.CallableFileWriter;
import ru.mail.alexander_kurlovich3.task2.MaxOfGeneretedArrayCallableService;
import ru.mail.alexander_kurlovich3.task3.FileReaderThread;
import ru.mail.alexander_kurlovich3.task3.FileRunableWriterOfRandomInt;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {
        //task1
        String taskName = "Task1 File1.txt";
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(taskName))) {
            for (int i = 0; i < 10; i++) {
                bw.write("С:\\WebServers\\home\\testsite\\www\\myfile" + i + ".txt");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> listOfNames = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(taskName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                listOfNames.add(line.substring(line.lastIndexOf('\\') + 1, line.indexOf('.')));
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Callable<Long> threadForWriting = new CallableFileWriter(listOfNames);
        Future<Long> resultOfWriting = executorService.submit(threadForWriting);
        try {
            System.out.println("Filr size = " + resultOfWriting.get() + " bytes");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();

        //task2
        ExecutorService executorService1 = Executors.newFixedThreadPool(4);
        Callable<Integer> threadForMaxValueOfRandomInt = new MaxOfGeneretedArrayCallableService();
        List<Future<Integer>> listOfMaxValues = new ArrayList<>();
        int numOfThreads = 10;
        for (int i = 0; i < numOfThreads; i++) {
            listOfMaxValues.add(executorService1.submit(threadForMaxValueOfRandomInt));
        }
        executorService1.shutdown();
        long summ = 0;
        for (Future<Integer> maxValue : listOfMaxValues) {
            try {
                System.out.println(maxValue.get());
                summ += maxValue.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Average = " + summ / listOfMaxValues.size());

        //task3
        String task3FileName = new String("Task3.txt");
        Thread writeThread = new Thread(new FileRunableWriterOfRandomInt(task3FileName));
        writeThread.start();
        Thread readThread = new FileReaderThread(task3FileName);
        readThread.start();

    }
}
