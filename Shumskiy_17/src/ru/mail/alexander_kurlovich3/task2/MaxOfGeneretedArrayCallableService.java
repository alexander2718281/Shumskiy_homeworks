package ru.mail.alexander_kurlovich3.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

public class MaxOfGeneretedArrayCallableService implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        int numOFRandomInt = 10;
        Random random = new Random();
        List<Integer> listOfRandomInr = new ArrayList<>();
        int maxValue = -2147483648;
        for (int i = 0; i < numOFRandomInt; i++) {
            listOfRandomInr.add(random.nextInt());
            if (listOfRandomInr.get(i)>maxValue){
                maxValue = listOfRandomInr.get(i);
            }
        }
        return maxValue;
    }
}
