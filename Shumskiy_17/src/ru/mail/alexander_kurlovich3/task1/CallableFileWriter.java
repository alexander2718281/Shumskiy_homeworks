package ru.mail.alexander_kurlovich3.task1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.concurrent.Callable;

public class CallableFileWriter implements Callable<Long> {
    private List<String> listOfNames;

    public CallableFileWriter(List<String> listOfNames) {
        this.listOfNames = listOfNames;
    }

    @Override
    public Long call() throws Exception {
        File newFile = new File("Task1 File2.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile))){
            for (String listOfName : listOfNames) {
                bufferedWriter.write(listOfName);
                bufferedWriter.newLine();
            }
        }
        return newFile.length();
    }
}
