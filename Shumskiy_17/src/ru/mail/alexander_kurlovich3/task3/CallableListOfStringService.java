package ru.mail.alexander_kurlovich3.task3;

import java.util.List;
import java.util.concurrent.Callable;

public class CallableListOfStringService implements Callable<Long> {
    private List<String> listOfLines;

    public CallableListOfStringService(List<String> listOfLines) {
        this.listOfLines = listOfLines;
    }

    @Override
    public Long call() throws Exception {
        long summ = 0;
        for (String line : listOfLines) {
            String[] lines = line.split(", ");
            for (String s : lines) {
                int value = Integer.valueOf(s.trim());
                summ += value;
            }
        }
        System.out.println(Thread.currentThread().getName() + " " + summ);
        Thread.currentThread().sleep(1000);
        return summ;
    }
}
