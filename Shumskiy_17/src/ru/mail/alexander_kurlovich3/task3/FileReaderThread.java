package ru.mail.alexander_kurlovich3.task3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class FileReaderThread extends Thread {
    private String fileAdress;

    public FileReaderThread(String fileAdress) {
        this.fileAdress = fileAdress;
    }

    @Override
    public void run() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        List<Future<Long>> summs = new ArrayList<>();
        synchronized (fileAdress) {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileAdress))) {
                String line = bufferedReader.readLine();
                while (line != null) {
                    List<String> lines = new ArrayList<>();
                    for (int i = 0; i < 10; i++) {
                        lines.add(line);
                        line = bufferedReader.readLine();
                    }
                    Callable<Long> threadForStringToSumm = new CallableListOfStringService(lines);
                    summs.add(executorService.submit(threadForStringToSumm));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
        long maxSumm = 0;
        try {
            maxSumm = summs.get(0).get();
            for (Future<Long> summ : summs) {
                if (summ.get() > maxSumm) {
                    maxSumm = summ.get();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("Max value of summ = " + maxSumm);
    }
}
