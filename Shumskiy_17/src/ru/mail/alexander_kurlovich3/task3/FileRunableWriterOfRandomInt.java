package ru.mail.alexander_kurlovich3.task3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class FileRunableWriterOfRandomInt implements Runnable {
    private String fileAdress;

    public FileRunableWriterOfRandomInt(String fileAdress) {
        this.fileAdress = fileAdress;
    }

    @Override
    public void run() {
        Random random = new Random();
        int numOfLines = 1000;
        synchronized (fileAdress) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileAdress))) {
                for (int i = 0; i < numOfLines; i++) {
                    bufferedWriter.write(random.nextInt() + ", " + random.nextInt() + ", " + random.nextInt() + ", " + random.nextInt());
                    bufferedWriter.newLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
