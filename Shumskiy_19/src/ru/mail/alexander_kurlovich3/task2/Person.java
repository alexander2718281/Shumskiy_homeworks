package ru.mail.alexander_kurlovich3.task2;

import java.util.Comparator;
import java.util.Objects;

public final class Person implements Comparable<Person> {
    private String name;
    private String surname;
    private int yearOfBeatth;

    private Person(Builder builder){
        this.name = builder.name;
        this.surname = builder.surname;
        this.yearOfBeatth = builder.yearOfBeatth;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYearOfBeatth() {
        return yearOfBeatth;
    }

    @Override
    public int compareTo(Person o) {
        if (this.yearOfBeatth != o.getYearOfBeatth()){
            return this.yearOfBeatth - o.getYearOfBeatth();
        }
        else if(this.surname != o.getSurname()){
            return this.surname.compareTo(o.getSurname());
        }
        else {
            return this.name.compareTo(o.getName());
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return yearOfBeatth == person.yearOfBeatth &&
                Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, surname, yearOfBeatth);
    }

    public static Builder newPersonBuilder(){
        return new Builder();
    }

    public static final class Builder{
        private String name;
        private String surname;
        private int yearOfBeatth;

        private Builder(){}

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withSurmame(String surname){
            this.surname = surname;
            return this;
        }

        public Builder withYearOfBearth(int yearOfBeatth){
            this.yearOfBeatth = yearOfBeatth;
            return this;
        }

        public Person build(){
            return new Person(this);
        }
    }
}
