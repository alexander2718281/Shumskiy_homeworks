package ru.mail.alexander_kurlovich3;

import ru.mail.alexander_kurlovich3.task1.ListOfIntFactory;
import ru.mail.alexander_kurlovich3.task1.Type;
import ru.mail.alexander_kurlovich3.task2.Person;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        //task1
        ListOfIntFactory factory = new ListOfIntFactory();
        List<Integer> list = factory.generateListOfRandomInt(Type.ARRAY_LIST, 50, 20);
        System.out.println(list.getClass().getSimpleName() + " of random int:");
        for (Integer integer : list) {
            System.out.println(integer);
        }

        //task2
        Set<Person> persons = new TreeSet<>();
        int numOfPersons = 10;
        Random random = new Random();
        Person.Builder builder = Person.newPersonBuilder();
        for (int i = 0; i < numOfPersons; i++) {
            persons.add(builder.withName("Name" + i).withSurmame("Surname" + i).withYearOfBearth(random.nextInt(100) + 1918).build());
        }
        System.out.println("Persons:");
        for (Person person : persons) {
            System.out.println(person.getYearOfBeatth() + " " + person.getSurname() + " " + person.getName());
        }
    }
}
