package ru.mail.alexander_kurlovich3.task1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ListOfIntFactory {
    Random random = new Random();

    public List<Integer> generateListOfRandomInt(Type type, int numOfElements, int maxValue){
        List<Integer> list;
        if (type == Type.ARRAY_LIST){
            list = new ArrayList<>(numOfElements);
            for (int i = 0; i < numOfElements; i++) {
                list.add(random.nextInt(maxValue));
            }
        }
        else if (type == Type.LINKED_LIST){
            list = new LinkedList<Integer>();
            for (int i = 0; i < numOfElements; i++) {
                list.add(random.nextInt(maxValue));
            }
        }
        else {
            throw new IllegalStateException();
        }
        return list;
    }
}

